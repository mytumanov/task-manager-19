package ru.mtumanov.tm.command.user;

import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.model.User;

public class UserViewProfileCommand extends AbstractUserCommand {

    @Override
    public String getDescription() {
        return "view current user profile";
    }

    @Override
    public String getName() {
        return "view-user-profile";
    }

    @Override
    public void execute() throws AbstractException {
        final User user = serviceLocator.getAuthService().getUser();
        System.out.println("[USER VIEW PROFILE]");
        showUser(user);        
    }
    
}
