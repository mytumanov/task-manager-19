package ru.mtumanov.tm.command.user;

import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.model.User;
import ru.mtumanov.tm.util.TerminalUtil;

public class UserRegistryCommand extends AbstractUserCommand {

    @Override
    public String getDescription() {
        return "registry user";
    }

    @Override
    public String getName() {
        return "user-registry";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[USER REGISTRY]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        final String password = TerminalUtil.nextLine();
        System.out.println("ENTER EMAIL:");
        final String email = TerminalUtil.nextLine();
        final User user = serviceLocator.getAuthService().registry(login, password, email);
        showUser(user);
    }

}
