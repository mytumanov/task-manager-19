package ru.mtumanov.tm.command.project;

import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.model.Project;
import ru.mtumanov.tm.util.TerminalUtil;

public class ProjectRemoveByIndexCommand extends AbstractProjectCommand {

    @Override
    public String getDescription() {
        return "Remove project by index";
    }

    @Override
    public String getName() {
        return  "project-remove-by-index";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[REMOVE PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = getProjectService().findOneByIndex(index);
        getProjectTaskService().removeProjectById(project.getId());
    }
    
}
