package ru.mtumanov.tm.command.task;

import java.util.List;

import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.model.Task;
import ru.mtumanov.tm.util.TerminalUtil;

public class TaskShowByProjectIdCommand extends AbstractTaskCommand {

    @Override
    public String getDescription() {
        return "Show tasks by project id";
    }

    @Override
    public String getName() {
        return "task-show-by-project-id";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[TASK LIST BY PROJECT ID]");
        System.out.println("ENTER PROJECT ID:");
        final String projectId = TerminalUtil.nextLine();
        final List<Task> tasks = getTaskService().findAllByProjectId(projectId);
        renderTask(tasks);
    }
    
}
