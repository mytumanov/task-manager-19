package ru.mtumanov.tm.comparator;

import java.util.Comparator;

import ru.mtumanov.tm.api.model.IHaveStatus;

public enum StatusComparator implements Comparator<IHaveStatus> {

    INSTANCE;

    @Override
    public int compare(final IHaveStatus o1, final IHaveStatus o2) {
        if (o1 == null || o2 == null) return 0;
        if (o1.getStatus() == null || o2.getStatus() == null) return 0;
        return o1.getStatus().compareTo(o2.getStatus());
    }

}
