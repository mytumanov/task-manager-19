package ru.mtumanov.tm.api.service;

import java.util.Comparator;
import java.util.List;

import ru.mtumanov.tm.enumerated.Sort;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.model.AbstractModel;

public interface IService<M extends AbstractModel> {

    List<M> findAll();

    M add(M model) throws AbstractException;

    List<M> findAll(Comparator<M> comparator);

    List<M> findAll(Sort sort);

    M findOneById(String id) throws AbstractException;

    M findOneByIndex(Integer index) throws AbstractException;

    M remove(M model) throws AbstractException;

    M removeById(String id) throws AbstractException;

    M removeByIndex(Integer index) throws AbstractException;

    void clear();

}
