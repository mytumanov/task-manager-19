package ru.mtumanov.tm.api.service;

import ru.mtumanov.tm.enumerated.Status;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.model.Project;

public interface IProjectService extends IService<Project> {

    Project create(String name, String description) throws AbstractException;

    Project updateById(String id, String name, String description) throws AbstractException;

    Project updateByIndex(Integer index, String name, String description) throws AbstractException;

    Project changeProjectStatusByIndex(Integer index, Status status) throws AbstractException;

    Project changeProjectStatusById(String id, Status status) throws AbstractException;

}
