package ru.mtumanov.tm.api.repository;

import java.util.List;

import ru.mtumanov.tm.model.Task;

public interface ITaskRepository extends IRepository<Task> {

    List<Task> findAllByProjectId(String projectId);

}
