package ru.mtumanov.tm.api.repository;

import ru.mtumanov.tm.enumerated.Sort;
import ru.mtumanov.tm.exception.entity.AbstractEntityNotFoundException;
import ru.mtumanov.tm.model.AbstractModel;

import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    M add(M model);

    List<M> findAll();

    List<M> findAll(Comparator<M> comparator);

    List<M> findAll(Sort sort);

    M findOneById(String id);

    M findOneByIndex(Integer index);

    M remove(M model);

    M removeById(String id) throws AbstractEntityNotFoundException;

    M removeByIndex(Integer index) throws AbstractEntityNotFoundException;

    void clear();

    int getSize();

    boolean existById(String id);

}
