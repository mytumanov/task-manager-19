package ru.mtumanov.tm.api.repository;

import java.util.Collection;

import ru.mtumanov.tm.command.AbstractCommand;

public interface ICommandRepository {

    Collection<AbstractCommand> getTerminalCommands();

    void add(AbstractCommand abstractCommand);

    AbstractCommand getCommandByName(String name);

    AbstractCommand getCommandByArgument(String arg);

}
