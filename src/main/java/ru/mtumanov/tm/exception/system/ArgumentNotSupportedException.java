package ru.mtumanov.tm.exception.system;

public final class ArgumentNotSupportedException extends AbstractSytemException {

    public ArgumentNotSupportedException() {
        super("ERROR! Argument not supported!");
    }

    public ArgumentNotSupportedException(final String arg) {
        super("ERROR! Argument: " + arg + " not supported!");
    }

}
