package ru.mtumanov.tm.repository;

import ru.mtumanov.tm.api.repository.IRepository;
import ru.mtumanov.tm.enumerated.Sort;
import ru.mtumanov.tm.exception.entity.AbstractEntityNotFoundException;
import ru.mtumanov.tm.exception.entity.EntityNotFoundException;
import ru.mtumanov.tm.model.AbstractModel;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    protected final List<M> models = new ArrayList<>();

    @Override
    public M add(final M model) {
        models.add(model);
        return model;
    }

    @Override
    public List<M> findAll() {
        return models;
    }

    @Override
    public List<M> findAll(final Comparator<M> comparator) {
        final List<M> result = new ArrayList<>(models);
        result.sort(comparator);
        return result;
    }

    @Override
    public List<M> findAll(final Sort sort) {
        return findAll(sort.getComparator());
    }

    @Override
    public M findOneById(final String id) {
        for (final M model : models) {
            if (id.equals(model.getId())) return model;
        }
        return null;
    }

    @Override
    public M findOneByIndex(final Integer index) {
        return models.get(index);
    }

    @Override
    public M remove(final M model) {
        models.remove(model);
        return model;
    }

    @Override
    public M removeById(final String id) throws AbstractEntityNotFoundException {
        final M model = findOneById(id);
        if (model == null) throw new EntityNotFoundException();
        models.remove(model);
        return model;
    }

    @Override
    public M removeByIndex(final Integer index) throws AbstractEntityNotFoundException {
        final M model = models.get(index);
        if (model == null) throw new EntityNotFoundException();
        models.remove(model);
        return model;
    }

    @Override
    public void clear() {
        models.clear();
    }

    @Override
    public int getSize() {
        return models.size();
    }

    @Override
    public boolean existById(final String id) {
        return findOneById(id) != null;
    }

}
