package ru.mtumanov.tm.repository;

import java.util.ArrayList;
import java.util.List;

import ru.mtumanov.tm.api.repository.ITaskRepository;
import ru.mtumanov.tm.model.Task;

public final class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    @Override
    public List<Task> findAllByProjectId(final String projectId) {
        final List<Task> taskList = new ArrayList<>();
        for (final Task task : models) {
            if (task.getProjectId() == null) continue;
            if (task.getProjectId().equals(projectId)) taskList.add(task);
        }
        return taskList;
    }

}
